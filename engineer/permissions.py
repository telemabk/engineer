from django.contrib.auth.models import Group

from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.created_by == request.user


class HasGroupPermission(permissions.BasePermission):
    """
    Ensure user is in required groups.
    """

    def is_in_group(self, user, group_name):
        return Group.objects.get(name=group_name)\
            .user_set.filter(id=user.id).exists()

    def has_permission(self, request, view):
        # return True
        required_groups_mapping = getattr(view, 'required_groups', {})
        required_groups = required_groups_mapping.get(request.method, [])
        print 'xd'
        print all([self.is_in_group(request.user, group_name) for group_name in required_groups])
        return all([self.is_in_group(request.user, group_name) for group_name in required_groups])
