from rest_framework import viewsets, permissions
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.response import Response

from .serializers import (
    ExtendedArticleSerializer,
    ExtendedActivitySerializer)
from .models import Article, Activity


class ArticleViewSet(viewsets.ModelViewSet):
    """
    API for articles
    """
    queryset = Article.objects.all()
    serializer_class = ExtendedArticleSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user,
                        updated_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)


class ActivityViewSet(viewsets.GenericViewSet,
                      RetrieveModelMixin):
    """
    API for activity
    """
    queryset = Activity.objects.all()
    serializer_class = ExtendedActivitySerializer

    def list(self, request):
        queryset = self.get_queryset()
        serializer = ExtendedActivitySerializer(
            queryset, many=True, context={'request': request})
        return Response(serializer.data)
