from django.db import models

from engineer.utils import now, timedelta
from engineer.userprofile.models import UserProfile
from engineer.api.models import Photo


# ####################
#   Main models      #
# ####################

class Hobby(models.Model):
    title = models.CharField(max_length=200, blank=False, default='')


class Activity(models.Model):
    CHOICES = [('undefined', 'undefined'), ('article', 'article'),
               ('thing', 'thing'), ('routine', 'routine'), ('news', 'news'),
               ('task', 'task'), ('video', 'video'), ('place', 'place')]

    hobby = models.ForeignKey(Hobby, related_name='hobby', blank=True,
                              null=True, default=None)
    kind = models.CharField(max_length=10, choices=CHOICES, default='undefined')
    score = models.IntegerField(default=10)
    description = models.TextField(blank=True, default=u'')
    updated_at = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(UserProfile,
                                   related_name='last_updated_activity')
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(UserProfile,
                                   related_name='last_created_activity')

    class Meta:
        ordering = ('created_at',)


# ####################
# Activity models    #
# ####################

class Article(models.Model):
    title = models.CharField(max_length=200, blank=False)
    source_url = models.URLField(max_length=400, blank=True)
    content = models.TextField(blank=False)

    image = models.ForeignKey(Photo, blank=True, null=True, default=None,
                              on_delete=models.CASCADE)
    activity = models.ForeignKey(Activity, related_name='article',
                                 on_delete=models.CASCADE)

    class Meta:
        ordering = ('title',)


class Place(models.Model):
    title = models.CharField(max_length=200, blank=False, default='')
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    www = models.URLField(max_length=400, blank=True)
    activity = models.ForeignKey(Activity, default=u'', related_name='place')

    class Meta:
        ordering = ('title',)


class Thing(models.Model):
    title = models.CharField(max_length=200, blank=False, default='')
    description = models.TextField(blank=True, default=u'')
    image = models.ForeignKey(Photo)
    www = models.URLField(max_length=400, blank=True)
    activity = models.ForeignKey(Activity, default=u'', related_name='thing')

    class Meta:
        ordering = ('title',)


class Routine(models.Model):
    REPEATS = [('day', 'every day'), ('week', 'every week'),
               ('month', 'every month'), ('weekday', 'every weekdays'),
               ('weekend', 'every weekend')]

    title = models.CharField(max_length=200, blank=False, default='')
    description = models.TextField(blank=True, default=u'')
    duration = models.IntegerField(blank=False, default=30)
    repeats = models.CharField(max_length=15, choices=REPEATS, default='week')
    image = models.ForeignKey(Photo)
    www = models.URLField(max_length=400, blank=True)
    activity = models.ForeignKey(Activity, default=u'', related_name='routine')

    class Meta:
        ordering = ('title',)


class News(models.Model):
    title = models.CharField(max_length=200, blank=False, default='')
    www = models.URLField(max_length=400, blank=False)
    start_date = models.DateTimeField(default=now())
    end_data = models.DateTimeField(default=timedelta(days=7))
    activity = models.ForeignKey(Activity, default=u'', related_name='news')

    class Meta:
        ordering = ('start_date',)


class Task(models.Model):
    title = models.CharField(max_length=200, blank=False, default='')
    description = models.TextField(blank=True, default=u'')
    image = models.ForeignKey(Photo)
    activity = models.ForeignKey(Activity, default=u'', related_name='task')

    class Meta:
        ordering = ('title',)


class Video(models.Model):
    title = models.CharField(max_length=200, blank=False, default='')
    www = models.URLField(max_length=400, blank=False)
    activity = models.ForeignKey(Activity, default=u'', related_name='video')

    class Meta:
        ordering = ('title',)
