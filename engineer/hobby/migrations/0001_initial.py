# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kind', models.CharField(default=b'undefined', max_length=10, choices=[(b'undefined', b'undefined'), (b'article', b'article'), (b'thing', b'thing'), (b'routine', b'routine'), (b'news', b'news'), (b'task', b'task'), (b'video', b'video'), (b'place', b'place')])),
                ('score', models.IntegerField(default=10)),
                ('description', models.TextField(default='', blank=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.ForeignKey(related_name='last_created_activity', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('created_at',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('source_url', models.URLField(max_length=400, blank=True)),
                ('content', models.TextField()),
                ('activity', models.ForeignKey(related_name='article', to='hobby.Activity')),
                ('image', models.ForeignKey(default=None, blank=True, to='api.Photo', null=True)),
            ],
            options={
                'ordering': ('title',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Hobby',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'', max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'', max_length=200)),
                ('www', models.URLField(max_length=400)),
                ('start_date', models.DateTimeField(default=datetime.datetime(2015, 11, 11, 15, 18, 21, 402000))),
                ('end_data', models.DateTimeField(default=datetime.datetime(2015, 11, 18, 15, 18, 21, 402000))),
                ('activity', models.ForeignKey(related_name='news', default='', to='hobby.Activity')),
            ],
            options={
                'ordering': ('start_date',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'', max_length=200)),
                ('longitude', models.DecimalField(max_digits=9, decimal_places=6)),
                ('latitude', models.DecimalField(max_digits=9, decimal_places=6)),
                ('www', models.URLField(max_length=400, blank=True)),
                ('activity', models.ForeignKey(related_name='place', default='', to='hobby.Activity')),
            ],
            options={
                'ordering': ('title',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Routine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'', max_length=200)),
                ('description', models.TextField(default='', blank=True)),
                ('duration', models.IntegerField(default=30)),
                ('repeats', models.CharField(default=b'week', max_length=15, choices=[(b'day', b'every day'), (b'week', b'every week'), (b'month', b'every month'), (b'weekday', b'every weekdays'), (b'weekend', b'every weekend')])),
                ('www', models.URLField(max_length=400, blank=True)),
                ('activity', models.ForeignKey(related_name='routine', default='', to='hobby.Activity')),
                ('image', models.ForeignKey(to='api.Photo')),
            ],
            options={
                'ordering': ('title',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'', max_length=200)),
                ('description', models.TextField(default='', blank=True)),
                ('activity', models.ForeignKey(related_name='task', default='', to='hobby.Activity')),
                ('image', models.ForeignKey(to='api.Photo')),
            ],
            options={
                'ordering': ('title',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Thing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'', max_length=200)),
                ('description', models.TextField(default='', blank=True)),
                ('www', models.URLField(max_length=400, blank=True)),
                ('activity', models.ForeignKey(related_name='thing', default='', to='hobby.Activity')),
                ('image', models.ForeignKey(to='api.Photo')),
            ],
            options={
                'ordering': ('title',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'', max_length=200)),
                ('www', models.URLField(max_length=400)),
                ('activity', models.ForeignKey(related_name='video', default='', to='hobby.Activity')),
            ],
            options={
                'ordering': ('title',),
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='activity',
            name='hobby',
            field=models.ForeignKey(related_name='hobby', default=None, blank=True, to='hobby.Hobby', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='activity',
            name='updated_by',
            field=models.ForeignKey(related_name='last_updated_activity', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
