from rest_framework import serializers

from .models import Activity, Article


class ActivitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Activity
        fields = ('url', 'hobby', 'score', 'description', 'kind',
                  'created_by', 'created_at', 'updated_by', 'updated_by')
        read_only_fields = ('kind', 'created_by', 'updated_by', 'hobby')


class ExtendedActivitySerializer(serializers.HyperlinkedModelSerializer):
    detail = serializers.SerializerMethodField()

    class Meta:
        model = Activity
        fields = ('url', 'hobby', 'score', 'description', 'kind', 'detail',
                  'created_by', 'created_at', 'updated_by', 'updated_by')
        read_only_fields = ('kind', 'created_by', 'updated_by', 'hobby')

    def get_detail(self, obj):
        detail = getattr(obj, obj.kind).first()
        if detail:
            request = self.context['request']
            if isinstance(detail, Article):
                serializer = ArticleSerializer(
                    detail, context={'request': request})
            # add more case
            return serializer.data


class ArticleSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Article
        fields = ('url', 'title', 'source_url', 'content', 'image')


class ExtendedArticleSerializer(serializers.HyperlinkedModelSerializer):
    activity = ActivitySerializer()

    class Meta:
        model = Article
        fields = ('url', 'title', 'source_url', 'content', 'image', 'activity')

    def create(self, validated_data):
        activity_data = validated_data.pop('activity')
        activity_data['created_by'] = validated_data.pop('created_by')
        activity_data['updated_by'] = validated_data.pop('updated_by')
        activity = Activity.objects.create(kind='article', **activity_data)
        article = Article.objects.create(activity=activity, **validated_data)
        return article

    def update(self, instance, validated_data):
        # save related activity
        activity_data = validated_data.pop('activity')
        activity_data['updated_by'] = validated_data.pop('updated_by')
        activity = instance.activity
        activity.score = activity_data.get('score', activity.score)
        activity.description = activity_data.get('description', activity.description)
        activity.save()
        instance.title = validated_data.get('title', instance.title)
        instance.source_url = validated_data.get('source_url', instance.source_url)
        instance.content = validated_data.get('content', instance.content)
        instance.save()
        return instance
