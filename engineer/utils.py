import datetime


def now():
    return datetime.datetime.now()


def timedelta(days=7):
    return datetime.datetime.now() + datetime.timedelta(days=days)
