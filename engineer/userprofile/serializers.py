from django.contrib.auth.models import Group
from rest_framework import serializers

from engineer.social.serializers import FriendRequestSerializer
from engineer.userprofile.models import UserProfile


class UserSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    url = serializers.HyperlinkedIdentityField(view_name='userprofile-detail')
    username = serializers.CharField(error_messages={'required':'Please Type a Username'})
    password = serializers.CharField(write_only=True)
    confirmed_password = serializers.CharField(write_only=True)
    old_password = serializers.CharField(allow_null=True, required=False)
    email = serializers.EmailField(error_messages={'required':'Please Type a email'})
    friends = serializers.HyperlinkedIdentityField(view_name='userprofile-detail',many=True, required=False, read_only=True)
    requests_pending = FriendRequestSerializer(many=True, read_only=True)
    requests_recived = FriendRequestSerializer(many=True, read_only=True)
    conversation_participant = serializers.HyperlinkedIdentityField(view_name='conversation-detail',many=True, required=False, read_only=True)
    avatar = serializers.ImageField(required=False)#serializers.ImageField(required=False, allow_null=True)

    class Meta:
        model = UserProfile
        fields = ('id', 'username', 'password', 'email', 'friends')
        read_only_fields = ('id',)

    def validate_username(self, username):
        if  username == '':
            raise serializers.ValidationError("Nick cannot be empty!")
        if self.Meta.model.objects.filter(username=username).exists():
            raise serializers.ValidationError("Username {username} already exists".format(username=username))
        return username

    def validate_password(self, password):
        if password == '':
            raise serializers.ValidationError("passwords must be longer")
        return password

    def validate_confirmed_password(self, confirmed_password):
        if confirmed_password == '':
            raise serializers.ValidationError("confirmed_password must be longer")
        return confirmed_password

    def validate(self, data):
        if data.has_key('password') and data.has_key('confirmed_password'):
            if data['password'] != data['confirmed_password']:
                raise serializers.ValidationError("passwords not match")
        return data

    def validate_email(self, email):
        if email == '':
            raise serializers.ValidationError("Email cannot be empty!")
        elif self.Meta.model.objects.filter(email=email).exists():
            raise serializers.ValidationError("Email {email} already in database".format(email=email))
        return email

    def create(self, validated_data):
        if UserProfile.objects.filter(username=validated_data['username']).exists():
            pass
        else:
            user = UserProfile.objects.create(
                username=validated_data['username'],
                email=validated_data['email'],
            )
            default_group = Group.objects.get_or_create(name='user')
            user.groups = default_group
            user.set_password(validated_data['password'])
            user.save()
        return user

    def update(self, instance, validated_data):
        #Change password
        if validated_data.has_key('old_password'):
            if validated_data['old_password'] == instance.password:
                    instance.password = validated_data['password']
        #Change nickname

        #Chage Email
        return instance


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')