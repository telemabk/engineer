from django.contrib import auth
from django.contrib.auth import login
from django.contrib.auth.models import Group
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from rest_framework import viewsets,filters
import rest_framework
from rest_framework.decorators import api_view, detail_route
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.serializers import HyperlinkedModelSerializer
from rest_framework.status import HTTP_201_CREATED
from rest_framework import permissions

from engineer.permissions import HasGroupPermission
from engineer.social.models import Conversation
from engineer.social.serializers import ConversationSerializer
from engineer.userprofile import services
from engineer.userprofile.models import UserProfile
from engineer.userprofile.serializers import UserSerializer, GroupSerializer

## RESTapi views
from engineer.userprofile.services import get_users_connection


class SearchResultsSetPagination(PageNumberPagination):
    page_size = 3
    page_size_query_param = 'page_size'
    max_page_size = 100

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = UserProfile.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('username', )
    permission_classes = [rest_framework.permissions.AllowAny]
    http_method_names = ['get', 'post', 'head', 'delete', 'options','patch']
    required_groups = {
        'GET': ['user'],
        'POST': [],
        'PUT': ['admin'],
        'DELETE': ['admin']
    }

    @detail_route()
    def friends(self, request, pk=None):
        user = UserProfile.objects.filter(pk=pk)[0]
        response_data = []
        for friend in user.friends.all():
            friend_json = UserSerializer(friend, context={'request': request}).data
            for conversation in friend.conversation_participant.all():
                if conversation.participants.filter(username=friend.username)[0]:
                    friend_json['conversation_id'] = conversation.id

            response_data.append(friend_json)
        return Response(status=rest_framework.status.HTTP_200_OK, data= response_data)

    @detail_route()
    def conversation(self, request, pk):
        '''
        Find conversation beetwen response_user and pk
        IF it not exist - create it!
        '''

        #same user
        if str(request.user.id) == pk:
            return Response(status=rest_framework.status.HTTP_400_BAD_REQUEST)

        # check in user conversation - maybe it exist : )
        private_conversation = request.user.conversation_participant.filter(private=True, participants=pk)
        if private_conversation:
            private_conversation = private_conversation[0]
        else:
            private_conversation = Conversation.objects.create(private=True)
            other_user = UserProfile.objects.filter(pk=pk)[0]
            private_conversation.participants.add(request.user)
            private_conversation.participants.add(other_user)
            request.user.conversation_participant.add(private_conversation)
            other_user.conversation_participant.add(private_conversation)


        response_data =  ConversationSerializer(private_conversation).data
        return Response(status=rest_framework.status.HTTP_200_OK, data= response_data)

class UserDetailView(RetrieveUpdateDestroyAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [HasGroupPermission]
    required_groups = {
        'GET': ['admin'],
        'POST': ['admin'],
        'PUT': ['admin'],
        'DELETE': ['admin']
    }

## Django views

def user_login(request):
    context = RequestContext(request)
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    else:
        if request.method == 'POST':
            username = request.POST.get('username', '')
            password = request.POST.get('password', '')
            user = auth.authenticate(username=username, password=password)
            if user:
                token = services.api_get_token(username, password)
                request.session['token'] = token
                login(request, user)
                return HttpResponseRedirect('/', {}, context)
            else:
                return HttpResponse("Invalid login details supplied.")

        else:
            return render_to_response('welcome_page/content/login.html', {}, context)

def user_logout(request):
    auth.logout(request)
    return render_to_response('welcome_page/content/main_page.html')

def user_register(request):
    context = RequestContext(request)
    if request.method == 'POST':
        response = services.api_create_user(request.POST)
        if response.status_code == HTTP_201_CREATED:
            return render_to_response('welcome_page/content/login.html', {}, context)
        else:
            return render_to_response('welcome_page/content/register.html',{'errors': response.json()} , context,)

    else:
        return render_to_response('welcome_page/content/register.html', {}, context)

def main_page(request):
    context = RequestContext(request)
    if request.user.is_authenticated():
        return redirect('/dashboard')
    else:
        return render_to_response('welcome_page/content/main_page.html', {}, context)


@api_view(['GET'])
def user_profile(request, username):
    context = RequestContext(request)
    token = request.session['token']
    user = get_object_or_404(UserProfile, username=username)

    user_api = services.api_get_user_by_id(token, request.user.id)
    profie_user_api = services.api_get_user_by_id(token, user.id)

    user_connections = get_users_connection(user_api, profie_user_api)


    return render_to_response('user_panel/content/user_profile.html', {
        'profile_user_data': profie_user_api.json(),
        'users_connection': user_connections},
                              context)