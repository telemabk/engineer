import os
from django.contrib.auth.models import AbstractUser, AbstractBaseUser
from django.db import models
from rest_framework.authtoken.models import Token
# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from engineer.api.models import Photo


class UserProfile(AbstractUser):
    avatar = models.ImageField(upload_to = 'avatar', default='photo/default.jpg'
                              # height_field = 'photo_height',
                              # width_field = 'photo_width'
                              )
    friends = models.ManyToManyField("UserProfile", blank=True, null=True)
    requests_pending = models.ManyToManyField("social.FriendRequest", blank=True, null=True, related_name="friend_request_send")
    requests_recived = models.ManyToManyField("social.FriendRequest", blank=True, null=True, related_name="friend_request_recived")
    conversation_participant = models.ManyToManyField("social.Conversation", blank=True, null=True, related_name="conversations_participant")

    def add_friend(self, friend):
        self.friends.add(friend)

    def add_friend_request(self, friend_request):
        self.requests_recived.add(friend_request)

    def add_requests_pending(self, request):
        self.requests_pending.add(request)

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

