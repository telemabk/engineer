from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User
from rest_framework.reverse import reverse
from rest_framework.authtoken.models import Token
import json
from engineer.userprofile.models import UserProfile


class ApiTest(TestCase):
    def setUp(self):
        pass#self.c = Client()

    #
    # def test_authorization(self):
    #     header = {'HTTP_AUTHORIZATION': 'Token {}'.format(self.token)}
    #     #response = self.client.get(reverse('bookmark-list'), {}, **header)
    #     #self.assertEqual(response.status_code, 200, "REST token-auth failed")

    def create_user_json(self):
        json = {
        "username": "Rafal",
        "password": "Rafal1",
        "confirmed_password": "Rafal1",
        "email": "lizonr@gmail.com"
    }
        return json


    def test_create_user(self):
        #header = {'HTTP_AUTHORIZATION': 'Token {}'.format(self.token)}
        response = self.client.post(reverse('userprofile-list'), self.create_user_json())
        self.assertEqual(response.status_code, 201, "User create failed")