import requests
from engineer.settings import API_URL

def api_get_user_by_id(token, id):
    url_new = API_URL + 'api/users/{id}/'.format(id=id)
    response = requests.get(url_new, headers={'Authorization': 'Token {token}'.format(token=token)})
    return response

def api_create_user(user_data):
    url_new = API_URL + 'api/users/'
    response = requests.post(url_new, data = user_data)
    return response

def api_get_token(username, password):
    url_new = API_URL + 'api-token-auth/'
    response = requests.post(url_new ,data={'username':username, 'password':password})
    return response.json()['token']

def get_users_connection(api_user, api_other_user):
    if (api_other_user.json()['url'] in  api_user.json()['friends']):
        return ["FRIEND", '/users/'+api_other_user.json()['username']]
    elif (api_other_user.json()['url'] in [request['target'] for request in api_user.json()['requests_pending']]):
        return ["PENDING", '/users/'+api_other_user.json()['username']]
    elif (api_user.json()['url'] in [request['target'] for request in api_user.json()['requests_recived']]):
        request_id = [request['id'] for request in api_user.json()['requests_recived']][0]
        return ["ACCEPT", '/friend_request/{request_id}/accept'.format(request_id=request_id)]
    elif (api_other_user.json()['username'] == api_user.json()['username']):
        return ['OWNER','/users/'+api_other_user.json()['username']]
    else:
        None
