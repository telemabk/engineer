from django.db import models


class Photo(models.Model):
    image = models.ImageField(upload_to="photo")
    title = models.CharField(max_length=200, default=u'photo')

    def __str__(self):
        return '{} - {}'.format(self.title, self.image)


