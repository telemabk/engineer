from django.contrib.sites.models import get_current_site
from rest_framework import serializers

from .models import Photo


class PhotoSerializer(serializers.HyperlinkedModelSerializer):
    photo_url = serializers.SerializerMethodField()
    image = serializers.ImageField(use_url=False, write_only=True)

    class Meta:
        model = Photo
        fields = ('photo_url', 'image', 'title', 'url')

    def get_photo_url(self, obj):
        request = self.context.get('request')
        full_url = ''.join(['http://', get_current_site(request).domain, obj.image.url])
        return full_url
