from rest_framework import viewsets
from rest_framework.parsers import FormParser, MultiPartParser

from .models import Photo
from .serializers import PhotoSerializer


class PhotoViewSet(viewsets.ModelViewSet):
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer
    parser_classes = (FormParser, MultiPartParser,)
