from django.contrib import admin

# Register your models here.
from engineer.api.models import Photo

admin.site.register(Photo)