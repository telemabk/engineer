from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from rest_framework import routers
from rest_framework.authtoken import views as token_view

from api import views as api_views
from hobby import views as hobby_views
from social import views as social_views
from userprofile import views as user_views

router = routers.DefaultRouter()
router.register(r'users', user_views.UserViewSet)
router.register(r'groups', user_views.GroupViewSet)
router.register(r'article', hobby_views.ArticleViewSet)
router.register(r'photos', api_views.PhotoViewSet)
router.register(r'friend_request', social_views.FriendRequestViewSet)
router.register(r'activity', hobby_views.ActivityViewSet)
# router.register(r'text_messages', social_views.TextMessageViewSet)
router.register(r'conversation', social_views.ConversationViewSet)


urlpatterns = [
    url(r'^$', user_views.main_page),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth/', token_view.obtain_auth_token),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', user_views.user_login),
    url(r'^logout/$', user_views.user_logout),
    url(r'^register/$', user_views.user_register),
    url(r'^dashboard/$', social_views.dashboard),
    url(r'^profile/$', social_views.profile),
    url(r'^search/$', social_views.Search.as_view()),
    url(r'^users/(?P<username>\w+)/$', user_views.user_profile),
    url(r'^users/(?P<user_id>\w+)/conversation/$', social_views.conversation_detail),
    url(r'^friend_request/(?P<friend_username>\w+)/$', social_views.friend_request),
    url(r'^conversation/(?P<conversation_id>\w+)/$', social_views.conversation_detail),
    url(r'^conversation/$', social_views.conversation),
    url(r'^friend_request/(?P<friend_request_id>\w+)/accept/$', social_views.friend_request_accept),
    url(r'^friends/$', social_views.show_friends),
    url(r'^hobby/$', social_views.hobby_list),
    url(r'^hobby/(?P<hobby_id>\w+)/$$', social_views.hobby_detail),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)