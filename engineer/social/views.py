from django.core.urlresolvers import resolve
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from rest_framework import viewsets, permissions
import rest_framework
from rest_framework.decorators import api_view, detail_route
from rest_framework.response import Response
from rest_framework.views import APIView

from engineer.permissions import IsOwnerOrReadOnly
from engineer.social.serializers import (FriendRequestSerializer, TextMessageSerializer, ConversationSerializer)
from engineer.social.services import api_create_friend_request, api_accept_friend_request,  \
    api_get_friend_request, api_get_user_friends, api_add_message_to_conversation, api_get_conversation_bettwen_users, \
    api_find_user_by_text, api_get_conversation_messages, api_get_conversation_by_id
from engineer.userprofile import services
from engineer.userprofile.models import UserProfile
from engineer.userprofile.serializers import UserSerializer
from engineer.userprofile.services import api_get_user_by_id

from .models import FriendRequest, TextMessage, Conversation


class TextMessageViewSet(viewsets.ModelViewSet):
    serializer_class = TextMessageSerializer
    queryset = TextMessage.objects.all()


class ConversationViewSet(viewsets.ModelViewSet):
    serializer_class = ConversationSerializer
    queryset = Conversation.objects.all()

    @detail_route(['post'])
    def add(self, request, pk=None):
        text = request.POST['text']
        id = request.POST['author']
        author = UserProfile.objects.filter(pk=id)[0]
        new_message = TextMessage.objects.create(author=author, text = text)
        new_message.save()
        conv = Conversation.objects.filter(id=pk)[0]
        conv.messages.add(new_message)

    @detail_route(['get'])
    def messages(self, request, pk):
        conversation = Conversation.objects.filter(pk=pk)[0]
        data = []
        for message in conversation.messages.all().order_by('-created_at'):
            message_json = TextMessageSerializer(message, context={'request': request}).data
            message_json['author'] = message.author.username
            data.append(message_json)
        return Response(status=rest_framework.status.HTTP_200_OK, data=data)

class FriendRequestViewSet(viewsets.ModelViewSet):
    """
    API for articles
    """
    queryset = FriendRequest.objects.all()
    serializer_class = FriendRequestSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly,)
    permission_classes = [rest_framework.permissions.AllowAny]
    http_method_names = ['get', 'post', 'head', 'delete', 'options','patch']
    required_groups = {
        'GET': ['user'],
        'POST': [],
        'PUT': ['admin'],
        'DELETE': ['admin']
    }

    def perform_create(self, serializer):
        new_friend_request = serializer.save(created_by=self.request.user)
        new_friend_request.create()

    @detail_route()
    def accept(self, request, pk=None):
        friend_request = FriendRequest.objects.filter(pk=pk)
        if friend_request:
            friend_request[0].accept()
            new_friend = friend_request[0].target
            created_by = friend_request[0].created_by
            return Response(status=rest_framework.status.HTTP_200_OK, data= {'new_friend_username': new_friend.username,
                                                                             'new_friend_id':new_friend.id,
                                                                             'created_by_username': created_by.username,
                                                                             'created_by_id': created_by.id,
                                                                             })
        else:
            return Response(status=rest_framework.status.HTTP_400_BAD_REQUEST)

    @detail_route()
    def decline(self, request, pk=None):
        request = FriendRequest.objects.filter(pk=pk)
        if request:
            request[0].delete()
            return Response(status=rest_framework.status.HTTP_200_OK)
        else:
            return Response(status=rest_framework.status.HTTP_400_BAD_REQUEST)

def dashboard(request):
    context = RequestContext(request)
    data = {}
    return render_to_response('user_panel/content/dashboard.html', data, context)

def conversation_detail(request, conversation_id=None, user_id=None):
    context = RequestContext(request)
    token = request.session['token']
    print "RAFAL"
    if conversation_id:
        conversation = api_get_conversation_by_id(token, conversation_id)
    if user_id:
        conversation = api_get_conversation_bettwen_users(token, user_id)
        conversation_id = conversation.json()['id']
    if request.method=="POST":
        text = request.POST['text']
        author = request.user.id
        api_add_message_to_conversation(token, author, text, conversation_id)
        return HttpResponseRedirect('/conversation/{conversation_id}'.format(conversation_id=conversation_id), {}, context)
    print conversation_id
    messages = api_get_conversation_messages(token, conversation_id)
    print messages.json()
    return render_to_response('user_panel/content/conversation_detail.html', {'data':messages.json()}, context)

def conversation(request):
    context = RequestContext(request)
    conversations = request.user.conversation_participant.all()
    return render_to_response('user_panel/content/conversation.html', {'data':conversations, 'user':request.user}, context)

def profile(request):
    context = RequestContext(request)
    token = request.session['token']
    response = api_get_user_by_id(token,request.user.id)
    user_data = response.json()
    return render_to_response('user_panel/content/profile.html', {'user_data': user_data}, context)

@api_view(['POST'])
def friend_request(request, friend_username):
    context = RequestContext(request)
    token = request.session['token']
    api_create_friend_request(token, request.user.username, friend_username)
    return HttpResponseRedirect('/users/{friend_username}'.format(friend_username=friend_username), {}, context)

@api_view()
def friend_request_accept(request, friend_request_id):
    context = RequestContext(request)
    token = request.session['token']
    response = api_accept_friend_request(token, friend_request_id)
    created_by_username = response.json()['created_by_username']
    return HttpResponseRedirect('/users/{created_by_username}'.format(created_by_username=created_by_username), {}, context)

def show_friends(request):
    context = RequestContext(request)
    token = request.session['token']
    response = api_get_user_friends(token, request.user.id)
    friends_data = response.json()
    return render_to_response('user_panel/content/friends.html', {'friends_data': friends_data}, context)

class Search(APIView):
    def get(self,request):
        context = RequestContext(request)
        return render_to_response('user_panel/content/search.html', {}, context)

    def post(self,request):
        context = RequestContext(request)
        token = request.session['token']
        search_text = request.POST['search_text']
        user = UserProfile.objects.filter(username__startswith =search_text)
        response = api_find_user_by_text(token, search_text)
        count = response.json()['count']
        data = response.json()['results']

        if user:
            return render_to_response('user_panel/content/search.html', {'friends_data': data, 'count':count}, context)
        else:
            return render_to_response('user_panel/content/search.html', {}, context)

def hobby_list(request):
    context = RequestContext(request)
    token = request.session['token']
    response = api_get_user_friends(token, request.user.id)
    hobby_data = ""#hobby_data = response.json()
    return render_to_response('user_panel/content/hobby_list.html', {'hobby_data': hobby_data}, context)

def hobby_detail(request, hobby_id):
    context = RequestContext(request)
    token = request.session['token']
    response = api_get_user_friends(token, request.user.id)
    hobby_data = ""#hobby_data = response.json()
    return render_to_response('user_panel/content/hobby_detail.html', {'hobby_data': hobby_data}, context)