from django.shortcuts import get_object_or_404
import requests
from engineer.settings import API_URL
from engineer.userprofile.models import UserProfile
from engineer.userprofile.serializers import UserSerializer
from engineer.userprofile.services import api_get_user_by_id


def api_create_friend_request(token, sender_username, target_username):
    sender = get_object_or_404(UserProfile, username=sender_username)
    friend = get_object_or_404(UserProfile, username=target_username)

    api_sender = api_get_user_by_id(token, sender.id)
    api_friend = api_get_user_by_id(token, friend.id)
    request_data = {
        'created_by' : api_sender.url,
        'target' : api_friend.url
    }
    url_new = API_URL + 'api/friend_request/'
    response = requests.post(url_new, data = request_data, headers={'Authorization': 'Token {token}'.format(token=token)})
    return response

def api_get_friend_request(token, friend_request_id=None):
    if friend_request_id:
        url_new = API_URL + 'api/friend_request/{friend_request_id}/'.format(friend_request_id=friend_request_id)
    else:
        url_new = API_URL + 'api/friend_request/'
    response = requests.get(url_new, headers={'Authorization': 'Token {token}'.format(token=token)})
    return response

def api_accept_friend_request(token, friend_request_id):
    url_new = API_URL + 'api/friend_request/{friend_request_id}/accept/'.format(friend_request_id=friend_request_id)
    response = requests.get(url_new, headers={'Authorization': 'Token {token}'.format(token=token)})
    return response

def api_get_user_friends(token, user_id):
    url_new = API_URL + 'api/users/{user_id}/friends/'.format(user_id=user_id)
    response = requests.get(url_new, headers={'Authorization': 'Token {token}'.format(token=token)})
    return response

def api_add_message_to_conversation(token, author, text, conversation_id):
    url_new = API_URL + 'api/conversation/{conversation_id}/add/'.format(conversation_id=conversation_id)
    response = requests.post(url_new, headers={'Authorization': 'Token {token}'.format(token=token)}, data={'text': text, 'author': author})
    return response

def api_get_conversation_by_id(token, conversation_id):
    url_new = API_URL + 'api/conversation/{conversation_id}'.format(conversation_id=conversation_id)
    response = requests.get(url_new, headers={'Authorization': 'Token {token}'.format(token=token)})
    return response

def api_get_conversation_bettwen_users(token, friend_user_id):
    url_new = API_URL + 'api/users/{friend_user_id}/conversation/'.format(friend_user_id=friend_user_id)
    response = requests.get(url_new, headers={'Authorization': 'Token {token}'.format(token=token)})
    return response

def api_get_conversation_messages(token, conversation_id):
    url_new = API_URL + 'api/conversation/{conversation_id}/'.format(conversation_id=conversation_id)
    response = requests.get(url_new, headers={'Authorization': 'Token {token}'.format(token=token)})
    return response

def api_find_user_by_text(token, text):
    url_new = API_URL + 'api/users/?search={text}'.format(text=text)
    response = requests.get(url_new, headers={'Authorization': 'Token {token}'.format(token=token)})
    return response

def api_get_hobby_list(token, user_id):
    #@TODO
    #url_new = API_URL + 'api/users/{user_id}/friends/'.format(user_id=user_id)
    #response = requests.get(url_new, headers={'Authorization': 'Token {token}'.format(token=token)})
    return None