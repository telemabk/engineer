from django.db import models


# Create your models here.
from django.utils import timezone
from django.utils.datetime_safe import datetime


class AbstractRequest(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey("userprofile.UserProfile", related_name='request creator', default=u'')
    target = ''

    class Meta:
        abstract = True
        ordering = ('created_at',)

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def __unicode__(self):
        return "{0} -> {1}".format(self.created_by, self.target)


class FriendRequest(AbstractRequest):
    target = models.ForeignKey("userprofile.UserProfile", related_name='target', default=u'')

    def create(self):
        self.created_by.add_requests_pending(self)
        self.target.add_friend_request(self)

    def accept(self):
        self.target.add_friend(self.created_by)
        self.created_by.add_friend(self.target)
        self.delete()

# class HobbyRequest(AbstractRequest):
#     target = models.ForeignKey(Hobby, related_name='target', default=u'')


class Conversation(models.Model):
    participants = models.ManyToManyField("userprofile.UserProfile", blank=True, null=True)
    messages = models.ManyToManyField("social.TextMessage", blank=True, null=True)
    private = models.BooleanField(default=False)

    def add_message(self, author, text):
        message = TextMessage.objects.create()
        message.author = author
        message.text = text
        self.messages.add(message)

class TextMessage(models.Model):
    author = models.ForeignKey("userprofile.UserProfile", related_name='author', default=u'')
    text = models.CharField(max_length=1000)
    created_at = models.DateTimeField(default=datetime.now)