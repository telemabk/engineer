from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from engineer.social.models import FriendRequest, TextMessage, Conversation
from engineer.userprofile.models import UserProfile


class FriendRequestSerializer(serializers.HyperlinkedModelSerializer):
    target = serializers.HyperlinkedRelatedField(view_name='userprofile-detail', queryset=UserProfile.objects.all())

    class Meta:
        model = FriendRequest
        read_only_fields= ('created_by',)
        fields = ('id', 'url', 'target','created_by')
        initial = ('created_by', )

    def create(self, validated_data):
        if validated_data.has_key('target') and validated_data.has_key('created_by'):
            if validated_data['target'] in validated_data['created_by'].friends.all():
                raise serializers.ValidationError("This friendship already exist")
            if validated_data['created_by'] == validated_data['target']:
                raise serializers.ValidationError("You can send requests only to other users!")
            if FriendRequest.objects.filter(created_by=validated_data['created_by'], target=validated_data['target']).exists():
                raise serializers.ValidationError("This request already exist")
        return FriendRequest.objects.create(**validated_data)


class ConversationUser(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('username','avatar' )


class TextMessageSerializer(serializers.HyperlinkedModelSerializer):
    #author = serializers.SlugRelatedField(slug_field='username', queryset=UserProfile.objects.all())
    author = ConversationUser()

    class Meta:
        model = TextMessage
        fields = ('author', 'text','created_at')

class ConversationSerializer(serializers.HyperlinkedModelSerializer):
    messages = TextMessageSerializer(many=True)
    participants = serializers.SlugRelatedField(many=True, slug_field='username', queryset=UserProfile.objects.all())

    class Meta:
        model = Conversation
        fields = ('id', 'messages', 'participants',)