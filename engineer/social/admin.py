from django.contrib import admin

# Register your models here.
from engineer.social.models import FriendRequest, TextMessage, Conversation

admin.site.register(FriendRequest)
admin.site.register(Conversation)
admin.site.register(TextMessage)